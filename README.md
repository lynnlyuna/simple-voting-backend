**Implement a system with the following interfaces:**
1. A web system for people to express their opinions on candidates with modern frontend framework.

2. A valid HKID no. is required for the voting. An HKID no. allow voting ONLY ONE candidate for each campaign.

3. Able to host more than one voting campaign.

4. Each voting campaign with a start and end time.

5. A web interface for displaying the result after the end time.

6. The campaign will not accept new vote after the end time.

7. A web interface for displaying the current count/vote for each candidate.

8. A line-chart/histogram/pie-chart visualizing how the vote distributed.

9. Unit test cases to cover critical paths.

10. Avoid duplicate/concurrent voting by HKID.

11. A list to display all voting campaign.

12. display campaigns within start/end time first and order by total no. of votes.

13. display most recent ended campaign afterward.

# Requirement tools/environment
1. Node.js version 8.x or 10.x.

2. Angular CLI

## Development Backend server
1. Install the node modules by using the command `npm install`.

2. Launch the server by using the command `node server`. If launch status ok, then can go to [Documentation](http://localhost:3000/documentation#/) and check the api list.

## Running unit tests

Run `npm test` to execute the unit tests via [Mocha](https://mochajs.org/).